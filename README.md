# Nodify [Open Source] # 

Nodify is an extensible visual scripting library for Unity. 
You can extend base nodes to create custom behaviors on which designers and level designers can use to script the game. 

![nodify.PNG](https://bitbucket.org/repo/7GbkzE/images/1396987735-nodify.PNG)


### Why is this open source? ###
Following the example of Unity3D beginning to open source their modules, I decided it would be beneficial to the community to have an open source visual scripting editor.

### Philosophy ##

There a couple strong tenants that I feel compelled to include in this project.

1. **Ease of Access ** This library should be free for developers, this will create a stronger community and engine tie-in.

2. **Unity First: ** use the strengths of the engine to lean against. For instance: Each node extends a base MonoBehaviour (for serialization)

3. **Community: ** allowing the community to develop and strengthen the product will help to establish a better tool.

### What makes this different from other visual scripting libraries? ###
Instead of showing huge nodes of information, options, variables etc. We allow the developer / designer to "expose" the fields and methods that they are interested in interacting with. This saves space, and visual fidelity.

### How do I get set up? ###
Please visit the WIKI often as it will grow in time!

### Should I use this in a commercial product? ###
As Nodify grows, it will change and become a better library. It definitely could be used, however be cautious as this is an evolving project.

### Version ###
Currently, the project exists at v0.5. This should be considered beta stage, although some refactoring may occur.

### Does this work in Unity5? ###
Yes

### How does this work? ###
Each node is derived from a base (MonoBehaviour) class. This allows the library to harness the serialization power for fields etc. Exposed fields and methods are created as "Anchors" also derived from a MonoBehaviour.
This keeps the node structure visible in your Hierarchy so you know what is going on at all times.

### License ###
Nodify is released under the New BSD license. For more information see here: http://en.wikipedia.org/wiki/BSD_licenses#Terms

### Downloads ###
I will upload .unitypackage builds of the project occasionally, however; if you want the most up to date version feel free to clone or download the repository.


### Contribution guidelines ###
* Fork the Repo and make your changes.
* Create pull requests and I will honor them when able.