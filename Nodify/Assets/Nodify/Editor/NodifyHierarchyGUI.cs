﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Nodify.Runtime;

namespace Nodify.Editor
{
    [InitializeOnLoad]
    public static class NodifyHierarchyGUI
    {
        static NodifyHierarchyGUI()
        {
            UnityEditor.EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyGUI;
        }

        public static void OnHierarchyGUI(int instanceID, Rect selectionRect)
        {
            GameObject obj = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

            if (obj != null)
            {
                if (obj.GetComponent<NodeGroup>())
                {
                    Vector2 textSize = EditorStyles.foldout.CalcSize(new GUIContent(obj.name));

                    Rect iconRect = new Rect(selectionRect.x + textSize.x - 10, selectionRect.y + 4, 10, 10);

                    if (obj.GetComponent<NodeGroup>() == NodifyEditorUtilities.currentSelectedGroup)
                    {
                        GUI.DrawTexture(iconRect, Resources.Load<Texture>("Icons/node_group_icon_hierarchy_selected"));
                    }
                    else
                    {
                        GUI.DrawTexture(iconRect, Resources.Load<Texture>("Icons/node_group_icon_hierarchy"));
                    }
                }
            }
        }
    }
}